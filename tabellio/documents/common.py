import random

from Products.CMFCore.utils import getToolByName

import tabellio.config.utils

class BasePublication(object):
    @property
    def date_str(self):
        try:
            return self.date.strftime('%d/%m/%Y')
        except AttributeError:
            return ''

    def get_authors_as_string(self):
        if not self.authors:
            return ''
        return ', '.join([x.to_object.title for x in self.authors if x.to_object])

    @property
    def authors_html_str(self):
        if not self.authors:
            return ''
        t = []
        for x in self.authors:
            if x.to_object is None: continue
            url = x.to_object.absolute_url()
            if 'ministres' in url:
                t.append(u'%s' % x.to_object.title)
            else:
                t.append(u'<a href="%s">%s</a>' % (url, x.to_object.title))
        return ', '.join(t)

    @property
    def topic_titles(self):
        from plone.i18n.normalizer.fr import normalizer
        topic_dict = tabellio.config.utils.get_topics_dict()
        def cmp_topic(x, y):
            return cmp(normalizer.normalize(x).lower(),
                       normalizer.normalize(y).lower())
        return sorted([topic_dict.get(x, [x])[0] for x in self.topics], cmp_topic)

    @property
    def polgroups_str(self):
        if not self.polgroups:
            return ''
        return ', '.join([x.to_object.title for x in self.polgroups])

    def related_elements(self, max_items=5, closeness=0.5):
        if not self.topics:
            return []

        topics = self.topics[:]
        # try to get other elements sharing the same topics
        catalog = getToolByName(self, 'portal_catalog')
        previous_result = None
        r = []
        while len(topics) > len(self.topics)*closeness:
            brains = catalog(portal_type=self.portal_type,
                             topics={'query': topics, 'operator': 'and'})
            if len(brains) > max_items:
                brains = list(brains)
                random.shuffle(brains)
                if previous_result:
                    brains.sort(lambda x,y: -cmp(x.dateDoc, y.dateDoc))
                    brains = list(previous_result) + brains
                r = brains[:max_items]
                break
            elif len(brains) > max_items/2:
                brains = list(brains)
                brains.sort(lambda x,y: -cmp(x.dateDoc, y.dateDoc))
                r = brains
                break
            previous_result = list(brains)
            previous_result.sort(lambda x,y: -cmp(x.dateDoc, y.dateDoc))
            del topics[random.randint(0, len(topics)-1)]
        doc_ids = [self.id]
        return [x for x in r if ((type(x.getId) is str) and x.getId or x.getId()) not in doc_ids]
