# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from zope.interface import implements

from plone.directives import form, dexterity
from plone.dexterity.content import Item
from z3c.relationfield.schema import RelationChoice, RelationList
from plone.formwidget.contenttree import ObjPathSourceBinder
from z3c.relationfield.interfaces import IHasRelations
from plone.namedfile.field import NamedBlobFile

from themis.fields import LegisSession

from tabellio.documents.interfaces import MessageFactory as _
from tabellio.config.utils import get_documents_path

import typenames

from common import BasePublication

class IDocument(form.Schema):
    title = schema.TextLine(title=_(u'Title'))
    no = schema.TextLine(title=_('Number'), required=False)
    session = LegisSession(title=_(u'Session'))
    nodoc = schema.TextLine(title=_('Secondary Number'), required=False)
    noannexe = schema.TextLine(title=_('Appendix Number'), required=False)
    date = schema.Date(title=_(u'Date'))
    authors = RelationList(title=_(u'Authors'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Author'),
                                                     source=ObjPathSourceBinder()))
    topics = schema.Set(title=_(u'Topics'), required=False,
                        value_type=schema.TextLine(title=_(u'Topic')))
    polgroups = RelationList(title=_(u'Political Groups'), default=[], required=False,
                             value_type=RelationChoice(title=_(u'Political Group'),
                                                     source=ObjPathSourceBinder()))
    doctype = schema.TextLine(title=_('Document Type'))
    file = NamedBlobFile(title=_('File'), required=False)
    file_image_id = schema.TextLine(title=_('File Image Id'), required=False)

    parent_dossier = RelationChoice(title=_(u'Parent File'), required=False,
                                    source=ObjPathSourceBinder())
    final_version = schema.Bool(title=_(u'Final Version'), required=False, default=False)
    publication_date = schema.Date(title=_(u'Publication Date'), required=False)
    adoption_date = schema.Date(title=_(u'Adoption Date'), required=False)
    moniteur_date = schema.Date(title=_(u'Moniteur Date'), required=False)

    # reserved for PFB
    adopted = schema.Bool(title=_(u'Adopted'), required=False, default=False)
    related_docs = RelationList(title=_(u'Related Documents'), default=[], required=False,
                    value_type=RelationChoice(title=_(u'Related Document'), source=ObjPathSourceBinder()))
    sanction_date = schema.Date(title=_(u'Sanction Date'), required=False)
    commission_status = schema.TextLine(title=_(u'Commission Status'), required=False)
    seance_vote = schema.TextLine(title=_(u'Seance Vote'), required=False)
    seance_vote_date = schema.Date(title=_(u'Seance Vote Date'), required=False)
    author_is_government = schema.Bool(title=_(u'Author is Government'), default=False, required=False)
    reporters = RelationList(title=_(u'Reporters'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Reporter'),
                                                     source=ObjPathSourceBinder()))
    speakers = RelationList(title=_(u'Speakers'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Speaker'),
                                                     source=ObjPathSourceBinder()))


class Document(Item, BasePublication):
    implements(IDocument, IHasRelations)

    @property
    def canonical_path(self):
        return '/' + get_documents_path() + '/' + self.id

    @property
    def doctype_str(self):
        if self.doctype is None: return _('Unknown')
        return typenames.MAPPING.get(self.doctype, self.doctype)

    @property
    def refno(self):
        if not self.no:
            return '(%s)' % self.session
        if self.nodoc:
            if self.noannexe:
                return u'%s (%s) - N° %s (Annexe %s)' % (
                                self.no, self.session, self.nodoc, self.noannexe)
            else:
                return u'%s (%s) - N° %s' % (self.no, self.session, self.nodoc)
        else:
            if self.noannexe:
                return '%s (%s) (Annexe %s)' % (self.no, self.session, self.noannexe)
            else:
                return '%s (%s)' % (self.no, self.session)

    @property
    def reftitle(self):
        if not self.no:
            return '%s (%s)' % (self.doctype_str, self.session)
        return u'%s - %s' % (self.doctype_str, self.refno)

    @property
    def associated_docs(self):
        if not self.parent_dossier:
            return []
        docs = []
        dossier = self.parent_dossier.to_object
        if not dossier:
            return None
        doc_ids = [self.id]
        for histoline in dossier.histolines:
            if not histoline.document: continue
            if histoline.document.to_object.getId() in doc_ids: continue
            doc = histoline.document.to_object
            docs.append(histoline.document.to_object)
            doc_ids.append(histoline.document.to_object.getId())
        return docs

    def related_elements(self, max_items=5, closeness=0.5):
        r = BasePublication.related_elements(self, max_items, closeness)
        doc_ids = [x.id for x in (self.associated_docs or [])]
        return [x for x in r if ((type(x.getId) is str) and x.getId or x.getId()) not in doc_ids]

    @property
    def publication_date_str(self):
        return self.publication_date.strftime('%d/%m/%Y')

    @property
    def adoption_date_str(self):
        return self.adoption_date.strftime('%d/%m/%Y')

    @property
    def moniteur_date_str(self):
        return self.moniteur_date.strftime('%d/%m/%Y')

    @property
    def sanction_date_str(self):
        return self.sanction_date.strftime('%d/%m/%Y')

    @property
    def seance_vote_date_str(self):
        return self.seance_vote_date.strftime('%d/%m/%Y')

    def get_authors_as_string(self):
        if self.author_is_government:
            return 'Gouvernement'
        return BasePublication.get_authors_as_string(self)
