# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from zope.schema import Field
from zope.schema.interfaces import IField, IFromUnicode
from zope.component import adapts, adapter
from zope.interface import implements, implementsOnly, implementer

from zope import component
from zope.app.intid.interfaces import IIntIds

from plone.directives import form, dexterity
from plone.dexterity.content import Item
from z3c.relationfield.schema import RelationChoice, RelationList
from z3c.relationfield.interfaces import IHasRelations
from z3c.relationfield import RelationValue

from plone.formwidget.contenttree import ObjPathSourceBinder

from themis.fields import LegisSession

from tabellio.documents.interfaces import MessageFactory as _

from z3c.form.browser.text import TextWidget
from z3c.form.interfaces import IFormLayer, IFieldWidget
from z3c.form.widget import FieldWidget

from tabellio.config.utils import get_questions_path

import typenames
from common import BasePublication

from dossier import IHistoLine, HistoLine

class IQuestion(form.Schema):
    title = schema.TextLine(title=_(u'Title'))
    date = schema.Date(title=_(u'Date'))
    no = schema.TextLine(title=_('Number'))
    session = LegisSession(title=_(u'Session'))
    state = schema.TextLine(title=_(u'State'), required=False)
    authors = RelationList(title=_(u'Authors'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Author'),
                                                     source=ObjPathSourceBinder()))
    questype = schema.TextLine(title=_('Type'))
    topics = schema.Set(title=_(u'Topics'), required=False,
                        value_type=schema.TextLine(title=_(u'Topic')))
    polgroups = RelationList(title=_(u'Political Groups'), default=[], required=False,
                             value_type=RelationChoice(title=_(u'Author'),
                                                     source=ObjPathSourceBinder()))
    commissions = RelationList(title=_(u'Commissions'), default=[], required=False,
                           value_type=RelationChoice(title=_(u'Commission'),
                                                     source=ObjPathSourceBinder()))
    histolines = schema.List(title=_(u'Historic Lines'),
                             value_type=HistoLine())

    # reserved for PFB
    related_docs = RelationList(title=_(u'Related Documents'), default=[], required=False,
                    value_type=RelationChoice(title=_(u'Related Document'), source=ObjPathSourceBinder()))
    written_answer = schema.TextLine(title=_(u'Written Answer'), required=False)
    seance_com_date = schema.Date(title=_(u'Seance/Commission Date'), required=False)


class Question(Item, BasePublication):
    implements(IQuestion, IHasRelations)

    def reset_histolines(self):
        self.histolines = []

    def add_histoline(self, date, title, authors, commission_title, docintid, document, doc_pages):
        h = HistoLine()
        h.date = date
        h.comment = title
        h.authors = authors
        if docintid:
            h.document = RelationValue(docintid)
        h.doc_pages = doc_pages
        if self.histolines and (
                        self.histolines[-1].date == h.date and
                        self.histolines[-1].title == h.title and
                        self.histolines[-1].authors == h.authors):
            self.histolines[-1] = h
        else:
            self.histolines.append(h)
        self.sync_participants()

    def sync_participants(self):
        author_intids = [x.to_id for x in self.authors or []]
        participants = []
        for histoline in self.histolines:
            if not histoline.authors:
                continue
            if not histoline.document:
                # if there's no document this is participation
                for x in histoline.authors:
                    if not x.to_id in participants:
                        participants.append(x.to_id)
            else:
                document = histoline.document.to_object
                if document and not document.authors:
                    # associated document has no authors, assume it's a summary
                    for x in document.authors:
                        if not x.to_id in participants:
                            participants.append(x.to_id)
        self.participants = [RelationValue(x) for x in participants]


    @property
    def canonical_path(self):
        return '/' + get_questions_path() + '/' + self.id

    @property
    def questype_str(self):
        if self.questype is None: return _('Unknown')
        return typenames.MAPPING.get(self.questype, self.questype)

    @property
    def state_str(self):
        return {'S_SYS_FIN': u'Travail du parlement terminé',
                'S_ACTIVE': u'Autre',
                'S_SYS_SCE': u'En séance',
                'S_FIN_GEL': u'Travail du parlement terminé',
                'S_SYS_COM': u'En commission',
                'S_FIN': u'Travail du parlement terminé',
                'S_COM': u'En commission',
                'S_SYS_ACTIVE': u'Autre',
                'S_SCE': u'En séance'}.get(self.state, u'Autre')

    @property
    def refno(self):
        if self.no:
            return '%s (%s)' % (self.no, self.session)
        else:
            return '(%s)' % self.session

    @property
    def reftitle(self):
        if self.no:
            return '%s - %s (%s)' % (self.questype_str, self.no, self.session)
        else:
            return '%s (%s)' % (self.questype_str, self.session)

    @property
    def date_str(self):
        if not self.date:
            return ''
        return self.date.strftime('%d/%m/%Y')

    @property
    def seance_com_date_str(self):
        if not self.date:
            return ''
        return self.seance_com_date.strftime('%d/%m/%Y')

    @property
    def associated_docs(self):
        docs = []
        doc_ids = []
        for histoline in self.histolines:
            if not histoline.document: continue
            if histoline.document.to_object.getId() in doc_ids: continue
            docs.append(histoline.document.to_object)
            doc_ids.append(histoline.document.to_object.getId())
        return docs

